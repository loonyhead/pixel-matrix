import React, { useState, useCallback } from "react";
import useWindowSize from './window';
import Box from './Box';
import { _MatrixConstructor, _RandColor } from './utils';
import './App.css';
const SIZE = 80;

function App() {

  const {width, height} = useWindowSize();
  const [matrix, setMatrix] = useState(_MatrixConstructor(width, height, SIZE));
  
  const onBoxClick = useCallback((i) => {
      let newMatrix = [...matrix];
      let { nodes } = newMatrix[i];
      let nextIndex = nodes[Math.floor(Math.random() * nodes.length)];
      newMatrix[i].color = _RandColor();
      newMatrix[nextIndex].color = _RandColor();
      setMatrix(newMatrix);
  }, []);

  return (
    <div className="App">
      {
        matrix.map((el,i)=>(
          <Box 
            onBoxClick={onBoxClick} 
            boxIndex={i} 
            bgColor={el.color}
            size={SIZE}
          />
        ))
      }
    </div>
  );
}

export default App;
