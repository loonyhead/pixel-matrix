import React from "react";

const Box = ({onBoxClick, boxIndex, bgColor, size}) => {
    return (
        <div 
            className="box" 
            style={{backgroundColor: bgColor, height: `${size}px`, width: `${size}px`}} 
            onClick={()=>onBoxClick(boxIndex)}
        />
    );
};

export default React.memo(Box);
