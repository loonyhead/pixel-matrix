export function _MatrixConstructor(w, l, s) {
    const x = Math.floor(w/s);
    const y = Math.floor(l/s);
    const xy = x*y;
    let matrix = [];
    for(let i=0; i<xy; i++){
      let nodes = [];
      let color = _RandColor();
      if(!(i%x === 0)) nodes.push(i-1);
      if(!((i+1)%x === 0)) nodes.push(i+1);
      if(i>=x) nodes.push(i-x);
      if(i< (xy-x)) nodes.push(i+x);
      matrix.push({
        nodes,
        color,
      });
    }
    return matrix;
}

export function _RandColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}